from __main__ import app
from datetime import timedelta
from flask_swagger_ui import get_swaggerui_blueprint
from flask import Flask, jsonify, request, abort
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity, get_jwt_claims
)

# import declared routes
from database import mysql

ACCESS = {
    'user': 0,
    'patient': 1,
    'doctor': 2,
    'admin' : 3
}

def get_role(number):
    switcher = {
        0: "ROLE_USER",
        1: "ROLE_PATIENT",
        2: "ROLE_DOCTOR",
        3: "ROLE_ADMIN"
    }
    return switcher.get(number, "Non existant")

class User():
    def __init__(self, username, password, access=ACCESS['user']):
        self.username = username
        self.password = password
        self.access = access
    
    def is_admin(self):
        return self.access == ACCESS['admin']
    
    def allowed(self, access_level):
        return self.access >= access_level
    
    def get_username(self):
        return self.username
    
    def get_password(self):
        return self.password
    
    def get_self(self):
        return self

USERS = []

USERS.append(User("jonas", "jonas", 1))
USERS.append(User("tomas", "tomas", 2))

# Setup the Flask-JWT-Extended extension
app.config['JWT_SECRET_KEY'] = 'arminas-kieciausias'  # Change this!
jwt = JWTManager(app)


# Using the user_claims_loader, we can specify a method that will be
# called when creating access tokens, and add these claims to the said
# token. This method is passed the identity of who the token is being
# created for, and must return data that is json serializable
@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    cur = mysql.connection.cursor()
    sql_update_query = ('''SELECT role FROM users WHERE username='%s';''' % (identity))
    check = cur.execute(sql_update_query)
    role = cur.fetchone()

    return {
        'Identity': identity,
        'Role': role
    }

# Provide a method to create access tokens. The create_access_token()
# function is used to actually generate the token, and you can return
# it to the caller however you choose.
@app.route('/auth', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
 
    cur = mysql.connection.cursor()
    
    sql_update_query = ('''SELECT * FROM users WHERE username='%s' AND password='%s';''' % (username, password))
    check = cur.execute(sql_update_query)
    results = cur.fetchone()

    if check == 0:
        return jsonify({"msg": "Bad username or password"}), 401

    ret = {
        'accessToken': create_access_token(identity=username, expires_delta=timedelta(minutes=10)),
        'id': results[0],
        'email' : results[3],
        'roles' : [
            {'role':get_role(results[4])}
        ],
        'username': username
    }
    return jsonify(ret), 200

    # Identity can be any data that is json serializable
    # access_token = create_access_token(identity=u.username, expires_delta=timedelta(seconds=50))
    # return jsonify(access_token=access_token), 200


# Protect a view with jwt_required, which requires a valid access token
# in the request to access.
@app.route('/protected', methods=['GET'])
@jwt_required
def protected():
    # Access the identity of the current user with get_jwt_identity
    claims = get_jwt_claims()
    return jsonify({
        'Identity': claims['Identity'],
        'Role': claims['Role']
    }), 200

    # current_user = get_jwt_identity()
    # return jsonify(logged_in_as=current_user), 200