from flask import Flask, abort, make_response, render_template, jsonify, request
from flask_mysqldb import MySQL
from flask_cors import CORS
# from werkzeug.exceptions import BadRequest
import mysql.connector
# from mysql.connector import Error
import json
# import traceback
# from logging import log
# import flask

app = Flask(__name__)

# import declared routes
import static.swagger
import errors.error_handle
import CRUDS.doctors, CRUDS.patients, CRUDS.visits, CRUDS.doctors_visits, CRUDS.users, auth
from database import mysql

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})
    
@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run(ssl_context='adhoc', port=3002, debug=True)
    #add "host='0.0.0.0' if running on vm
    # app.run()