from flask import abort, jsonify, request
from __main__ import app
from flask_jwt_extended import(jwt_required, get_jwt_claims)

# import declared routes
from database import mysql

def get_role(number):
    switcher = {
        0: "ROLE_USER",
        1: "ROLE_PATIENT",
        2: "ROLE_Doctor",
        3: "ROLE_ADMIN"
    }
    return switcher.get(number, "Non existant")

@app.route("/users", methods=['GET'])
@jwt_required
def all_users():
    claims = get_jwt_claims()
    role = claims['Role']
    for r in role : 
        if r != 3:
            return jsonify({'message': "Unauthorized"}), 401
    response_object = {'status': 'success'}
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM users''')
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))
    response_object['users'] = json_data
    cur.close()
    return jsonify(response_object)

@app.route("/users", methods=['POST'])
def post_user():
    response_object = {'message': 'User added successfully'}
    cur = mysql.connection.cursor()
    post_data = request.get_json()
    if (not post_data.get('username')
        or not post_data.get('password')
        or not post_data.get('email')):
        return jsonify({"msg": "Missing parameter"}), 400
    sql_insert_query = ('''INSERT INTO users(username, password, email, role)
                            VALUES('%s', '%s', '%s', 0);'''
                            % (post_data.get('username'), post_data.get('password'), post_data.get('email')))
    try :
        check = cur.execute(sql_insert_query)
        check = mysql.connection.commit()
    except:
        return jsonify({"message": "Username already exists"}), 409
    cur.close()
    return jsonify(response_object)

@app.route("/users/<username>", methods=['PUT'])
@jwt_required
def put_user(username):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    if (not post_data.get('username')
        or not post_data.get('email')):
        abort(400)
    if(post_data.get('username') != username):
        sql_search = ('''SELECT * FROM users WHERE username='%s' ''' % post_data.get('username'))
        check = cur.execute(sql_search)
        if check == 1:
            return jsonify({"message": "Username already exists"}), 409
    sql_update_query = ('''UPDATE users
                            SET username='%s', email='%s', role='%s'
                            WHERE username='%s';'''
                            % (post_data.get('username'), post_data.get('email'), post_data.get('role'), username))
    
    check = cur.execute(sql_update_query)
    if check == 0:
        return jsonify({"message": "Username not found"}), 404
    mysql.connection.commit()
    response_object['message'] = 'User updated'
    cur.close()
    return jsonify(response_object)

@app.route("/users/<username>", methods=['GET'])
@jwt_required
def get_user(username):
    cur = mysql.connection.cursor()
    response_object = {'message': 'success'}
    sql_select_query = '''SELECT * FROM users WHERE username='%s' ''' % username
    check = cur.execute(sql_select_query)
    if check == 0:
        abort(404)
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchone()
    ret = {
        'username': results[1],
        'email' : results[3],
        'roles' : [
            {'role':get_role(results[4])}
        ],
    }
    response_object['user'] = [ret]
    cur.close()
    return jsonify(ret), 200

@app.route("/users/<username>", methods=['DELETE'])
@jwt_required
def delete_user(username):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    sql_delete_query = '''DELETE FROM users WHERE username='%s' ''' % username
    check = cur.execute(sql_delete_query)
    mysql.connection.commit()
    if check == 0:
        return jsonify({"msg": "Username does not exist"}), 404
    response_object['message'] = 'User deleted'
    cur.close()
    return jsonify(response_object)