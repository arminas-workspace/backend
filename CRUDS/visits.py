from flask import abort, jsonify, request, json
from __main__ import app
from flask_jwt_extended import(jwt_required, get_jwt_claims)

# import declared routes
from database import mysql
import datetime

@app.route("/visits", methods=['GET', 'POST'])
@jwt_required
def all_visits():
    response_object = {'status': 'success'}
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        post_data = request.get_json()

        if post_data.get('idPatient') == None:
            sql_insert_query = ('''INSERT INTO visits(visitStart, visitEnd, idDoctor)
                                VALUES('%s', '%s', '%s');'''
                                % (post_data.get('visitStart'), post_data.get('visitEnd'), post_data.get('idDoctor')))
        else:
            sql_insert_query = ('''INSERT INTO visits(visitStart, visitEnd, idDoctor, idPatient)
                                VALUES('%s', '%s', '%s', '%s');'''
                                % (post_data.get('visitStart'), post_data.get('visitEnd'), post_data.get('idDoctor'), post_data.get('idPatient')))
        if (not post_data.get('visitStart')
            or not post_data.get('visitEnd')
            or not post_data.get('idDoctor')):
            abort(400)
        check = cur.execute(sql_insert_query)
        if check == 0:
            abort(404)
        check = mysql.connection.commit()
    else:
        cur.execute('''SELECT idVisit, DATE(visitStart) as day, TIME(visitStart) as visitStart, TIME(visitEnd) as visitEnd, idDoctor, idPatient FROM visits;''')
        row_headers=[x[0] for x in cur.description] #this will extract row headers
        results = cur.fetchall()
        json_data=[]
        for result in results:
            json_data.append(dict(zip(row_headers,result)))
        response_object['visits'] = json_data
    cur.close()
    return json.dumps(response_object, indent=4, sort_keys=True, default=str)
    #return jsonify(response_object)

@app.route("/visits/<visit_id>", methods=['PUT'])
@jwt_required
def single_visit(visit_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    if post_data.get('idPatient') != None:
        sql_update_query = ('''UPDATE visits
                                SET visitStart='%s', visitEnd='%s', idDoctor='%s', idPatient='%s'
                                WHERE idVisit=%s;'''
                                % (post_data.get('visitStart'), post_data.get('visitEnd'), post_data.get('idDoctor'), post_data.get('idPatient'), visit_id))
    else:
        sql_update_query = ('''UPDATE visits
                                SET visitStart='%s', visitEnd='%s', idDoctor='%s', idPatient=null
                                WHERE idVisit=%s;'''
                                % (post_data.get('visitStart'), post_data.get('visitEnd'), post_data.get('idDoctor'), visit_id))
    if (not post_data.get('visitStart')
        or not post_data.get('visitEnd')
        or not post_data.get('idDoctor')):
        abort(400)
    check = cur.execute(sql_update_query)
    if check == 0:
        abort(404)
    mysql.connection.commit()
    response_object['message'] = 'visits updated'
    cur.close()
    return jsonify(response_object)

@app.route("/visits/<doctor_id>", methods=['GET'])
@jwt_required
def single_visit_get(doctor_id):
    try:
        int(doctor_id)
    except ValueError:
        abort(400)
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    sql_select_query = '''SELECT idVisit, DATE(visitStart) as day, TIME(visitStart) as visitStart, TIME(visitEnd) as visitEnd, idDoctor, idPatient FROM visits WHERE idVisit=%s;''' % doctor_id
    check = cur.execute(sql_select_query)
    if check == 0:
        abort(404)
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))
    response_object['visits'] = json_data
    cur.close()
    return jsonify(response_object)

@app.route("/visits/<visit_id>", methods=['DELETE'])
@jwt_required
def single_visit_delete(visit_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    sql_delete_query = '''DELETE FROM visits WHERE idVisit=%s''' % visit_id
    check = cur.execute(sql_delete_query)
    mysql.connection.commit()
    if check == 0:
        abort(404)
    response_object['message'] = 'Visit deleted'
    cur.close()
    return jsonify(response_object)