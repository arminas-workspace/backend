from flask import abort, jsonify, request
from __main__ import app

# import declared routes
from database import mysql

@app.route("/doctors/<doctor_id>/visits", methods=['GET'])
def single_doctor_all_visits(doctor_id):
    try:
        int(doctor_id)
    except ValueError:
        abort(400)
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    sql_select_query = ('''SELECT visits.idVisit, visits.visitStart, visits.visitEnd, visits.idDoctor, visits.idPatient
                        FROM doctors
                        INNER JOIN visits ON doctors.idDoctor = visits.idDoctor
                        WHERE doctors.idDoctor=%s
                        order by doctors.idDoctor;'''% doctor_id)
    check = cur.execute(sql_select_query)
    if check == 0:
        abort(404)
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))
    response_object['visits'] = json_data
    cur.close()
    return jsonify(response_object)