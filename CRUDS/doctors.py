from flask import abort, jsonify, request
from __main__ import app
from flask_jwt_extended import(jwt_required, get_jwt_claims)

# import declared routes
from database import mysql


@app.route("/doctors", methods=['GET', 'POST'])
@jwt_required
def all_doctors():
    response_object = {'status': 'success'}
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        post_data = request.get_json()
        sql_insert_query = ('''INSERT INTO doctors(name, surname, phone, fk_idUser)
                             VALUES('%s', '%s', '%s', '%s');'''
                             % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('fk_idUser')))
        if(not post_data.get('name')
            or not post_data.get('phone')
            or not post_data.get('surname')
            or not post_data.get('fk_idUser')):
            abort(400)
        check = cur.execute(sql_insert_query)
        check = mysql.connection.commit()
    else:
        cur.execute('''SELECT * FROM doctors''')
        row_headers=[x[0] for x in cur.description] #this will extract row headers
        results = cur.fetchall()
        json_data=[]
        for result in results:
            json_data.append(dict(zip(row_headers,result)))
        response_object['doctors'] = json_data
    cur.close()
    return jsonify(response_object)


@app.route("/doctors/<doctor_id>", methods=['PUT'])
@jwt_required
def single_doctor(doctor_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    sql_update_query = ('''UPDATE doctors
                            SET name='%s', surname='%s', phone='%s', fk_idUser='%s'
                            WHERE idDoctor=%s;'''
                            % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('fk_idUser'), doctor_id))
    if (not post_data.get('name')
        or not post_data.get('surname')
        or not post_data.get('phone')
        or not post_data.get('fk_idUser')):
        abort(400)
    check = cur.execute(sql_update_query)
    if check == 0:
        abort(404)
    mysql.connection.commit()
    response_object['message'] = 'Doctors updated'
    cur.close()
    return jsonify(response_object)

@app.route("/doctors/<user_id>", methods=['GET'])
@jwt_required
def single_doctor_get(user_id):
    try:
        int(user_id)
    except ValueError:
        abort(400)
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    sql_select_query = '''SELECT * FROM doctors WHERE fk_idUser=%s''' % user_id
    check = cur.execute(sql_select_query)
    if check == 0:
        abort(404)
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))
    response_object['doctors'] = json_data
    cur.close()
    return jsonify(response_object)

@app.route("/doctors/<doctor_id>", methods=['DELETE'])
@jwt_required
def single_doctor_delete(doctor_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    sql_delete_query = ('''DELETE FROM doctors WHERE idDoctor=%s;''' % doctor_id)
    check = cur.execute(sql_delete_query)
    mysql.connection.commit()
    if check == 0:
        abort(404)
    response_object['message'] = 'Doctor deleted'
    cur.close()
    return jsonify(response_object)