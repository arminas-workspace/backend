from flask import abort, jsonify, request
from __main__ import app
from flask_jwt_extended import(jwt_required, get_jwt_claims)

# import declared routes
from database import mysql


@app.route("/patients", methods=['GET', 'POST'])
@jwt_required
def all_patients():
    response_object = {'status': 'success'}
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        post_data = request.get_json()
        if post_data.get('idDoctor') != None:
            sql_insert_query = ('''INSERT INTO patients(name, surname, phone, idDoctor, fk_idUser)
                             VALUES('%s', '%s', '%s', '%s', '%s');'''
                             % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('idDoctor'), post_data.get('fk_idUser')))
        else:
            sql_insert_query = ('''INSERT INTO patients(name, surname, phone, fk_idUser)
                             VALUES('%s', '%s', '%s', '%s');'''
                             % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('fk_idUser')))
        if (not post_data.get('name')
            or not post_data.get('surname')
            or not post_data.get('phone')
            or not post_data.get('fk_idUser')):
            abort(400)
        check = cur.execute(sql_insert_query)
        check = mysql.connection.commit()
    else:
        cur.execute('''SELECT * FROM patients''')
        row_headers=[x[0] for x in cur.description] #this will extract row headers
        results = cur.fetchall()
        json_data=[]
        for result in results:
            json_data.append(dict(zip(row_headers,result)))
        response_object['patients'] = json_data
    cur.close()
    return jsonify(response_object)

@app.route("/patients/<patient_id>", methods=['PUT'])
@jwt_required
def single_patient(patient_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    if post_data.get('idDoctor') != None:
        sql_update_query = ('''UPDATE patients
                                SET name='%s', surname='%s', phone='%s', idDoctor='%s', fk_idUser = '%s'
                                WHERE idPatient=%s;'''
                                % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('idDoctor'), post_data.get('fk_idUser'), patient_id))
    else:
        sql_update_query = ('''UPDATE patients
                                SET name='%s', surname='%s', phone='%s', fk_idUser = '%s'
                                WHERE idPatient=%s;'''
                                % (post_data.get('name'), post_data.get('surname'), post_data.get('phone'), post_data.get('fk_idUser'), patient_id))
    if (not post_data.get('name')
        or not post_data.get('surname')
        or not post_data.get('phone')):
        abort(400)
    check = cur.execute(sql_update_query)
    if check == 0:
        abort(404)
    mysql.connection.commit()
    response_object['message'] = 'Patients updated'
    cur.close()
    return jsonify(response_object)

@app.route("/patients/<fk_idUser>", methods=['GET'])
@jwt_required
def single_patient_get(fk_idUser):
    try:
        int(fk_idUser)
    except ValueError:
        abort(400)
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    sql_select_query = '''SELECT * FROM patients WHERE fk_idUser=%s''' % fk_idUser
    check = cur.execute(sql_select_query)
    if check == 0:
        abort(404)
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    results = cur.fetchall()
    json_data=[]
    for result in results:
        json_data.append(dict(zip(row_headers,result)))
    response_object['patients'] = json_data
    cur.close()
    return jsonify(response_object)

@app.route("/patients/<patient_id>", methods=['DELETE'])
@jwt_required
def single_patient_delete(patient_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    post_data = request.get_json()
    sql_delete_query = '''DELETE FROM patients WHERE idPatient=%s''' % patient_id
    check = cur.execute(sql_delete_query)
    mysql.connection.commit()
    if check == 0:
        abort(404)
    response_object['message'] = 'Patient deleted'
    cur.close()
    return jsonify(response_object)